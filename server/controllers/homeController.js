import models from '../models';

import Util from '../utils/Utils';

const util = new Util();

export default class HomeController {
  static home(req, res) {
    var data;
     try {
       data = models.User.findAll().then(function(users) {
        data = users;
        util.setSuccess(200, 'Data retrieved', users);
       });

       return util.send(res);
     } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }
}
