const fs = require('fs');

module.exports = {
  development: {
    username: 'root',
    password: 'root',
    database: 'felicitas_analytical',
    host: '127.0.0.1',
    port: 3306,
    dialect: 'mysql',
    dialectOptions: {
      bigNumberStrings: true
    },
    pool: {
        max: 20,
        min: 1,
        idle: 9000,
        acquire: 75000,
        evict: 700
    }
  },
  test: {
    username: process.env.CI_DB_USERNAME,
    password: process.env.CI_DB_PASSWORD,
    database: process.env.CI_DB_NAME,
    host: '127.0.0.1',
    port: 3306,
    dialect: 'mysql',
    dialectOptions: {
      bigNumberStrings: true
    },
    pool: {
        max: 20,
        min: 1,
        idle: 9000,
        acquire: 75000,
        evict: 700
    }
  },
  production: {
    username: process.env.PROD_DB_USERNAME,
    password: process.env.PROD_DB_PASSWORD,
    database: process.env.PROD_DB_NAME,
    host: process.env.PROD_DB_HOSTNAME,
    port: process.env.PROD_DB_PORT,
    dialect: 'mysql',
    dialectOptions: {
      bigNumberStrings: true,
      // ssl: {
      //   ca: fs.readFileSync(__dirname + '/mysql-ca-master.crt')
      // }
    },
    pool: {
        max: 20,
        min: 1,
        idle: 9000,
        acquire: 75000,
        evict: 700
    }
  }
};
