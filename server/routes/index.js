import express from 'express';
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log("Reached");
  res.status(200).send({
      success: 'true',
      message: 'todos retrieved successfully'
    })
});

export default router;
