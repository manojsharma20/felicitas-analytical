import express from 'express';

import HomeController from '../controllers/homeController';

var router = express.Router();

/* GET home page. */
router.get('/', HomeController.home);

export default router;
